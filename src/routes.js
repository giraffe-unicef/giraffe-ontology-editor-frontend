import React from "react";
import PositionsMapping from "./views/OntologyEditor/PositionsMapping/PositionsMapping";
import ProductServicesMapping from "./views/OntologyEditor/ProductServicesMapping/ProductServicesMapping";
import QualificationsMapping from "./views/OntologyEditor/QualificationsMapping/QualificationsMapping";
import SkillsMapping from "./views/OntologyEditor/SkillsMapping/SkillsMapping";
const PositionView = React.lazy(() =>
  import("./views/OntologyEditor/Position/Position")
);
const ProductsAndServicesEditor = React.lazy(() =>
  import("./views/OntologyEditor/ProductsAndServices/ProductsAndServicesEditor")
);
const QualificationEditor = React.lazy(() =>
  import("./views/OntologyEditor/Qualification/QualificationEditor")
);
const SkillEditor = React.lazy(() =>
  import("./views/OntologyEditor/Skill/SkillEditor")
);

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  
  {
    path: "/ontology/position",
    name: "Position",
    component: PositionView,    
  },  
  {
    path: "/ontology/product-service",
    name: "Products And Services",
    component: ProductsAndServicesEditor,    
  },
  {
    path: "/ontology/qualification",
    name: "Qualification",
    component: QualificationEditor,    
  },
  {
    path: "/ontology/skills",
    name: "Skills",
    component: SkillEditor,    
  },
  {
    path: "/ontology/products-services-mapping",
    name: "Products and Services Mapping",
    component: ProductServicesMapping,    
  },
  {
    path: "/ontology/qualifications-mapping",
    name: "Qualifications Mapping",
    component: QualificationsMapping,    
  },
  {
    path: "/ontology/skills-mapping",
    name: "Skills Mapping",
    component: SkillsMapping,    
  },
  {
    path: "/ontology/positions-mapping",
    name: "Positions Mapping",
    component: PositionsMapping,    
  }
];

export default routes;
