import React from "react";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import "react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit.min.css";
import "react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css";
import BootstrapTable from "react-bootstrap-table-next";
import { Button, Card, CardBody, Col, Row, FormGroup } from "reactstrap";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory from "react-bootstrap-table2-paginator";
import Modal from "react-bootstrap/Modal";

class SkillEditor extends  React.Component {
  constructor(props) {
    super(props);

    this.state = {
      dropdownOpen: false,
      radioSelected: 2,
      skills: [],
      loaded: false,
      showAddSkillModel: false,
      skillName: '',
      skillId: '',
    };
  }

  componentWillMount() {
    fetch(process.env.REACT_APP_EDITOR_API_URL + "/ontology/skills")
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          console.log(response);
        }
      })
      .then(json => this.setState({
        skills: json,
        loaded: true
      }));
  }
  hideModal = () => {
    this.setState({
      showAddSkillModel : false
    })
  }

  updateValue = (key,value) => {
    this.setState({
      [key] : value
    })
  }
  handleAddOrUpdateSkill = (event) => {
    event.preventDefault();
    let { skillName, skillId, action} = this.state;
    let requestOptions, url = "/ontology/skills";
    if(action === "EDIT"){
      requestOptions = { method: 'PUT', headers: { 'Content-Type': 'application/json' }, body: JSON.stringify({
        name : skillName
      }) };  
      url += `/${skillId}`
    }else{
      requestOptions = { method: 'POST', headers: { 'Content-Type': 'application/json' }, body: JSON.stringify({
        name : skillName      
      }) };  
    }
    

    fetch(process.env.REACT_APP_EDITOR_API_URL + url, requestOptions)
      .then(response => {
        if (response.ok) {
          this.componentWillMount();
          return response.json();
        } else {
          alert(response.message);
          console.log(response);
        }
      });
    this.hideModal();
  }

  renderAddSkillsModal() {    
    let { showAddSkillModel, skillName, skillId, action} = this.state;
    return (
      <>
        <Modal show={showAddSkillModel} onHide={this.hideModal}>
          <Modal.Header closeButton >
            <Modal.Title>Add Skill</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <form onSubmit={this.handleAddOrUpdateSkill}>
              {/* <FormGroup>
                <label htmlFor="position_id">Position Id</label>
                <input
                  type="number"
                  className="form-control"
                  id="position_id"
                  disabled
                  value={positionId || ""}
                />
              </FormGroup> */}
              <FormGroup>
                <label htmlFor="add_name">Skill Name</label>
                <input
                  type="text"
                  className="form-control"
                  id="add_name"
                  value={skillName}
                  onChange={(e) => {this.updateValue("skillName",e.target.value)}}
                />
              </FormGroup>

              <FormGroup>
                <button className="form-control btn btn-primary" type="submit">
                  Submit
                </button>
              </FormGroup>
            </form>
          </Modal.Body>
        </Modal>
      </>
    );
  }
  actionFormatter = (cell, row) => {
    return (
      <Col col="6" sm="2" md="2" xl className="mb-3 mb-xl-0">
        <Button
          renderas="button"
          block
          outline
          color="primary"
          onClick={() => this.handleEditActions(row)}
        >
          Edit
        </Button>
      </Col>
    );
  };

  handleEditActions = (row) => {    
    this.setState((prevState) => ({
      showAddSkillModel: true,
      skillName: row.name,
      skillId: row.id,      
      action: "EDIT",
    }));
  };

  handleAdd = () => {    
    this.setState((prevState) => ({
        showAddSkillModel: true,
        skillName: "",
        skillId: "",      
      action: "Add",
    }));
  };
  render() {
    const paginationSize = 10;
    let { skills } = this.state;
    let pagination = pagination = paginationFactory();
    const { SearchBar } = Search;
    let colStyle = {
      verticalAlign: "middle"
    };

    const columns = [
      {
        id : 0,
        dataField: "id",
        text: "Skill Id",
        headerStyle: {
          width: "10%"
        }
      },
      {
        id : 1,
        dataField: "name",
        text: "Skill Name",
        style: colStyle,
        sort: true,
        //formatter: dateFormatter,
        headerStyle: {
          width: "25%"
        }
      },
      {
        text: "Actions",
        align: "center",
        headerStyle: {
          width: "25%"
        },
        formatter: (cell, row, index) => this.actionFormatter(cell, row),
      }
    ];
    
    this.loadTable = ()  => {
        if(this.state.loaded){
          return (
              <ToolkitProvider
                  keyField="id"
                  data={skills}
                  columns={columns}
                  
                  style={{ overflowX: "scroll" }}
                  search
              >
                {props => (
                    <div>
                      <Row className="align-items-center">
                        <Col xs="12" lg="9">
                          <h2>Skills</h2>
                        </Col>
                        <Col xs="12" lg="3">
                          <SearchBar {...props.searchProps} />
                        </Col>
                        <Col xs="1">                          
                            
                              <Button
                                renderas="button"
                                block
                                outline
                                color="primary"
                                className="action-btn float-right "
                                onClick={this.handleAdd}
                              >
                                Add
                              </Button>                            
                          </Col>
                      </Row>
                      <hr />
                      <BootstrapTable
                          {...props.baseProps}
                          pagination={pagination}
                        />
                    </div>
                )}
              </ToolkitProvider>
          )
        }
      }

      return (
        <div className="animated fadeIn">
          <Card>
            <CardBody>
              <div className="d-none d-md-block">
              <Row className="align-items-center">
                <Col col="12" xl className="mb-3 mb-xl-0">
                  {this.loadTable()}
                </Col>
              </Row>
              </div>
              <div className="d-block d-md-none">
                  {this.loadTable()}
              </div>
            </CardBody>
          </Card>
          {this.renderAddSkillsModal()}
        </div>
      );
    }
  }
  
  export default SkillEditor;