import React, { Component } from "react";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import "react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit.min.css";
import "react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css";
import BootstrapTable from "react-bootstrap-table-next";
import { Button, Card, CardBody, Col, Row, FormGroup } from "reactstrap";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory from "react-bootstrap-table2-paginator";
import Modal from "react-bootstrap/Modal";
import Select from "react-select";
import * as _ from "lodash";
import { getPostions } from "../../../utilities/helperUtils";
class ProductServicesMapping extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      dropdownOpen: false,
      radioSelected: 2,
      productsServicesMapping: [],
      loaded: false,
      showAddPositionModel: false,
      agnosticProductsServices: null,
      industryIds: [],
      positionIds: [],
      productServiceIds: [],
      positions: [],
      industries: [],
      productsAndServices: [],
    };
  }

  componentWillMount() {
    Promise.all([
      fetch(process.env.REACT_APP_EDITOR_API_URL + "/ontology/positions"),
      fetch(process.env.REACT_APP_EDITOR_API_URL + "/industries"),
      fetch(
        process.env.REACT_APP_EDITOR_API_URL +
          "/ontology/products-and-services"
      ),
    ]).then(async (res) => {
      let positions = await res[0].json();
      this.setState({
        positions: positions,
        agnosticPositions : getPostions(positions,"agnostic_products_and_services", true),
        unAgnosticPositions : getPostions(positions,"agnostic_products_and_services", false),        
        industries: await res[1].json(),
        productsAndServices: await res[2].json(),
      });
      fetch(
        process.env.REACT_APP_EDITOR_API_URL +
          "/ontology/products-services-group-mapping"
      )
        .then((response) => {
          if (response.ok) {
            return response.json();
          } else {
            console.log(response);
          }
        })
        .then((json) => {
          for(let record of json){
            record.industryNames = this.getSelectedIndustriesNames(record).join(", ");
            record.positionNames = this.getSelectedPositionsNames(record).join(", ");
            record.productServiceNames = this.getSelectedProductsAndServicesNames(record).join(", ");
          }
          this.setState({
            productsServicesMapping: json,
            loaded: true,
          });
        });
    });
  }
  hideModal = () => {
    this.setState({
      showAddProductServicesMappingModel: false,
    });
  };

  updateValue = (key, value) => {
    this.setState({
      [key]: value,
    });
  };
  handleAddOrUpdateProductServicesMapping = (event) => {
    event.preventDefault();
    let { agnosticPandS, action } = this.state;
    let requestOptions,
      url = "/ontology/products-services-group-mapping";
    if (action === "EDIT") {
      requestOptions = {
        method: "PUT",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          agnostic_industry: agnosticPandS,
          industry_id: this.getSelectedIndustriesIds(),
          position_id: this.getSelectedPositionsId(),
          section_ids: this.getSelectedProductsAndServicesIds(),
          deleted_industry_ids: this.getDeletedIndustries(),
          deleted_position_ids: this.getDeletedPositions(),
          deleted_section_ids: this.getDeletedProductsAndServices(),
        }),
      };
    } else {
      requestOptions = {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          agnostic_industry: agnosticPandS,
          industry_id: this.getSelectedIndustriesIds(),
          position_id: this.getSelectedPositionsId()[0],
          section_ids: this.getSelectedProductsAndServicesIds(),
        }),
      };
    }

    fetch(process.env.REACT_APP_EDITOR_API_URL + url, requestOptions).then(
      (response) => {
        if (response.ok) {
          this.componentWillMount();
          return response.json();
        } else {
          alert(response.message);
          console.log(response);
        }
      }
    );
    this.hideModal();
  };

  agnosticDisabledCheck = () => {
    let disabled = null;
    for (let positon of this.state.currentSelectedPositions || []) {
      if (positon.agnostic_products_and_services) {
        disabled = true;
        break;
      }
    }
    return disabled;
  };

  agnosticValue = () => {
    let value = null;
    for (let positon of this.state.currentSelectedPositions || []) {
      if (positon.agnostic_products_and_services) {
        value = true;
        break;
      } else {
        value = false;
      }
    }
    return value;
  };

  getPostionsDropdown = () => {
    let {action, agnosticPandS, agnosticPositions, unAgnosticPositions, positions} = this.state;
    if(action === "EDIT"){
      if(agnosticPandS){
        return agnosticPositions;
      }else{
        return unAgnosticPositions;
      }
    }    
    return positions;
  }

  renderAddProductServiceMappingModal() {
    let {
      showAddProductServicesMappingModel,
      currentSelectedPositions,
      currentSelectedIndustries,
      currentSelectedProductsAndServices,
      agnosticPandS,
      action,
    } = this.state;
    return (
      <>
        <Modal
          show={showAddProductServicesMappingModel}
          onHide={this.hideModal}
        >
          <Modal.Header closeButton>
            <Modal.Title>Add Mapping</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <form onSubmit={this.handleAddOrUpdateProductServicesMapping}>
              {/* <FormGroup>
                <label htmlFor="position_id">Position Id</label>
                <input
                  type="number"
                  className="form-control"
                  id="position_id"
                  disabled
                  value={positionId || ""}
                />
              </FormGroup> */}
              <FormGroup>
                <label htmlFor="position">Positions</label>
                <Select
                  id="position"
                  isMulti={action === "EDIT"}
                  value={currentSelectedPositions}
                  options={this.getPostionsDropdown()}
                  getOptionValue={(option) => option.id}
                  getOptionLabel={(option) => option.description}
                  onChange={(e) => {
                    if (action !== "EDIT") {
                      this.updateValue("currentSelectedPositions", [e]);
                      this.updateValue(
                        "agnosticPandS",
                        e.agnostic_products_and_services
                      );
                    } else {
                      this.updateValue("currentSelectedPositions", e);
                      let agnostic = false;
                      for (let postion of e) {
                        if (postion.agnostic_products_and_services) {
                          agnostic = true;
                        }
                      }
                      this.updateValue("agnosticPandS", agnostic);
                    }
                  }}
                />
              </FormGroup>

              <FormGroup>
                <label htmlFor="agnostic_ps">
                  Agnostic Products And Service
                </label>
                <input
                  id="agnostic_ps"
                  name="agnostic_ps"
                  className="form-check-input ml-5"
                  type="checkbox"
                  checked={agnosticPandS}
                  disabled={this.agnosticDisabledCheck()}
                  onClick={() => {
                    if(!agnosticPandS){
                      this.updateValue("currentSelectedIndustries", []);
                    }
                    this.updateValue("agnosticPandS", !agnosticPandS);
                  }}
                />
              </FormGroup>

              <FormGroup>
                <label htmlFor="position">Industries</label>
                <Select
                  id="position"
                  isMulti={true}
                  isDisabled={agnosticPandS}
                  value={currentSelectedIndustries}
                  options={this.state.industries}
                  getOptionValue={(option) => option.id}
                  getOptionLabel={(option) => option.name}
                  onChange={(e) => {
                    this.updateValue("currentSelectedIndustries", e);
                  }}
                />
              </FormGroup>

              <FormGroup>
                <label htmlFor="position">Products and Services</label>
                <Select
                  id="p-s"
                  isMulti={true}
                  value={currentSelectedProductsAndServices}
                  options={this.state.productsAndServices}
                  getOptionValue={(option) => option.id}
                  getOptionLabel={(option) => option.name}
                  onChange={(e) => {
                    this.updateValue("currentSelectedProductsAndServices", e);
                  }}
                />
              </FormGroup>

              <FormGroup>
                <button className="form-control btn btn-primary" type="submit">
                  Submit
                </button>
              </FormGroup>
            </form>
          </Modal.Body>
        </Modal>
      </>
    );
  }
  actionFormatter = (cell, row) => {
    return (
      <Col col="6" sm="2" md="2" xl className="mb-3 mb-xl-0">
        <Button
          renderas="button"
          block
          outline
          color="primary"
          onClick={() => this.handleEditActions(row)}
        >
          Edit
        </Button>
      </Col>
    );
  };

  handleEditActions = (row) => {
    this.setState((prevState) => ({
      selectedRow: row,
      showAddProductServicesMappingModel: true,
      mappingId: row.id,
      currentSelectedPositions: this.getSelectedPositions(row.positionIds),
      currentSelectedIndustries: this.getSelectedIndustries(row.industryIds),
      currentSelectedProductsAndServices: this.getSelectedProductsAndServices(
        row.productServiceIds
      ),
      agnosticPandS: row.agnosticProductsServices,
      action: "EDIT",
    }));
  };

  handleAdd = () => {
    this.setState((prevState) => ({
      showAddProductServicesMappingModel: true,
      currentSelectedPositions: [],
      currentSelectedIndustries: [],
      currentSelectedProductsAndServices: [],
      agnosticPandS: null,
      action: "Add",
    }));
  };

  getSelectedPositions = (positionIds) => {
    let positions = [];
    for (let position of this.state.positions) {
      if (positionIds.indexOf(position.id) !== -1) {
        positions.push(position);
      }
    }
    return positions;
  };

  getSelectedPositionsId = () => {
    let positionIds = [];
    for (let position of this.state.currentSelectedPositions) {
      positionIds.push(position.id);
    }
    return positionIds;
  };

  getSelectedPositionsNames = (row) => {
    let positionNames = [];
    for (let position of this.state.positions) {
      if (row.positionIds.indexOf(position.id) !== -1) {
        positionNames.push(position.description);
      }
    }
    return positionNames;
  };

  getSelectedIndustries = (industryIds) => {
    let industries = [];
    for (let industry of this.state.industries) {
      if (industryIds.indexOf(industry.id) !== -1) {
        industries.push(industry);
      }
    }
    return industries;
  };

  getSelectedIndustriesIds = () => {
    let industryIds = [];
    for (let industry of this.state.currentSelectedIndustries) {
      industryIds.push(industry.id);
    }
    return industryIds;
  };

  getSelectedIndustriesNames = (row) => {
    let industryNames = [];    
    for (let industry of this.state.industries) {
      if (row.industryIds.indexOf(industry.id) !== -1) {
        industryNames.push(industry.name);
      }
    }
    return industryNames;
  };

  getSelectedProductsAndServices = (productServiceIds) => {
    let productsServices = [];
    for (let productsService of this.state.productsAndServices) {
      if (productServiceIds.indexOf(productsService.id) !== -1) {
        productsServices.push(productsService);
      }
    }
    return productsServices;
  };

  getSelectedProductsAndServicesIds = () => {
    let productsServiceIds = [];
    for (let productService of this.state.currentSelectedProductsAndServices) {
      productsServiceIds.push(productService.id);
    }
    return productsServiceIds;
  };

  getSelectedProductsAndServicesNames = (row) => {
    let productsServiceNames = [];
    for (let productService of this.state.productsAndServices) {
      if (row.productServiceIds.indexOf(productService.id) !== -1) {
        productsServiceNames.push(productService.name);
      }
    }
    return productsServiceNames;
  };
  getDeletedIndustries = () => {
    let selectedIndustries = this.getSelectedIndustriesIds();
    let oldIndustries = this.state.selectedRow.industryIds;
    let deletedIndustries = [];
    for (let industry of oldIndustries) {
      if (selectedIndustries.indexOf(industry) === -1) {
        deletedIndustries.push(industry);
      }
    }
    return deletedIndustries;
  };

  getDeletedPositions = () => {
    let selectedPositions = this.getSelectedPositionsId();
    let oldPositions = this.state.selectedRow.positionIds;
    let deletedPositions = [];
    for (let position of oldPositions) {
      if (selectedPositions.indexOf(position) === -1) {
        deletedPositions.push(position);
      }
    }
    return deletedPositions;
  };

  getDeletedProductsAndServices = () => {
    let selectedProductsAndServices = this.getSelectedProductsAndServicesIds();
    let oldProductsAndServices = this.state.selectedRow.productServiceIds;
    let deletedProductsAndServices = [];
    for (let productAndService of oldProductsAndServices) {
      if (selectedProductsAndServices.indexOf(productAndService) === -1) {
        deletedProductsAndServices.push(productAndService);
      }
    }
    return deletedProductsAndServices;
  };
  render() {
    const paginationSize = 10;
    let { productsServicesMapping } = this.state;
    let pagination = (pagination = paginationFactory());
    const { SearchBar } = Search;
    let colStyle = {
      verticalAlign: "middle",
    };

    const columns = [
      {
        id: 2,
        dataField: "positionNames",
        text: "Positions",
        style: colStyle,
        sort: true,
        //formatter: dateFormatter,
        headerStyle: {
          width: "25%",
        },
      },
      {
        id: 0,
        dataField: "agnosticProductsServices",
        text: "Agnostic Products Services",
        headerStyle: {
          width: "25%",
        },
      },
      {
        id: 1,
        dataField: "industryNames",
        text: "Industries",
        style: colStyle,
        sort: true,
        //formatter: dateFormatter,
        headerStyle: {
          width: "25%",
        },
      },
      
      {
        id: 3,
        dataField: "productServiceNames",
        text: "Product and Services",
        style: colStyle,
        sort: true,
        //formatter: dateFormatter,
        headerStyle: {
          width: "25%",
        },
      },
      {
        text: "Actions",
        align: "center",
        headerStyle: {
          width: "25%",
        },
        formatter: (cell, row, index) => this.actionFormatter(cell, row),
      },
    ];

    this.loadTable = () => {
      if (this.state.loaded) {
        return (
          <ToolkitProvider
            keyField="id"
            data={productsServicesMapping}
            columns={columns}
            style={{ overflowX: "scroll" }}
            search
          >
            {(props) => (
              <div>
                <Row className="align-items-center">
                  <Col xs="12" lg="9">
                    <h2>Products Services Group Mapping</h2>
                  </Col>
                  <Col xs="12" lg="3">
                    <SearchBar {...props.searchProps} />
                  </Col>
                  <Col xs="1">
                    <Button
                      renderas="button"
                      block
                      outline
                      color="primary"
                      className="action-btn float-right "
                      onClick={this.handleAdd}
                    >
                      Add
                    </Button>
                  </Col>
                </Row>
                <hr />
                <BootstrapTable {...props.baseProps} pagination={pagination} />
              </div>
            )}
          </ToolkitProvider>
        );
      }
    };

    return (
      <div className="animated fadeIn">
        <Card>
          <CardBody>
            <div className="d-none d-md-block">
              <Row className="align-items-center">
                <Col col="12" xl className="mb-3 mb-xl-0">
                  {this.loadTable()}
                </Col>
              </Row>
            </div>
            <div className="d-block d-md-none">{this.loadTable()}</div>
          </CardBody>
        </Card>
        {this.renderAddProductServiceMappingModal()}
      </div>
    );
  }
}

export default ProductServicesMapping;
