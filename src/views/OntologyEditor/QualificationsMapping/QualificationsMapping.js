import React, { Component } from "react";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import "react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit.min.css";
import "react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css";
import BootstrapTable from "react-bootstrap-table-next";
import { Button, Card, CardBody, Col, Row, FormGroup } from "reactstrap";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory from "react-bootstrap-table2-paginator";
import Modal from "react-bootstrap/Modal";
import Select from "react-select";
import { filter } from "lodash";
import { getPostions } from "../../../utilities/helperUtils";

class QualificationsMapping extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      dropdownOpen: false,
      radioSelected: 2,
      qualificationsMapping: [],
      loaded: false,
      showAddModel: false,
      agnosticQualification: null,
      industryIds: [],
      positionIds: [],
      qualificationIds: [],
      positions: [],
      industries: [],
      qualifications: [],
    };
  }

  componentWillMount() {
    Promise.all([
      fetch(process.env.REACT_APP_EDITOR_API_URL + "/ontology/positions"),
      fetch(process.env.REACT_APP_EDITOR_API_URL + "/industries"),
      fetch(
        process.env.REACT_APP_EDITOR_API_URL +
          "/ontology/qualifications"
      ),
    ]).then(async (res) => {      
      let positions = await res[0].json();
      this.setState({
        positions: positions,
        agnosticPositions : getPostions(positions,"agnostic_qualification", true),
        unAgnosticPositions : getPostions(positions,"agnostic_qualification", false),
        industries: await res[1].json(),
        qualifications: await res[2].json(),
      });
      fetch(
        process.env.REACT_APP_EDITOR_API_URL +
          "/ontology/qualifications-group-mapping"
      )
        .then((response) => {
          if (response.ok) {
            return response.json();
          } else {
            console.log(response);
          }
        })
        .then((json) => {
          for(let record of json){
            record.industryNames = this.getSelectedIndustriesNames(record).join(", ");
            record.positionNames = this.getSelectedPositionsNames(record).join(", ");
            record.qualificationNames = this.getSelectedQualificationsNames(record).join(", ");
          }
          this.setState({
            qualificationsMapping: json,
            loaded: true,
          });
        });
    });
  }
  hideModal = () => {
    this.setState({
      showAddModel: false,
    });
  };

  updateValue = (key, value) => {
    this.setState({
      [key]: value,
    });
  };
  handleAddOrUpdateQualificationsMapping = (event) => {
    event.preventDefault();
    let { agnostic, action } = this.state;
    let requestOptions,
      url = "/ontology/qualifications-group-mapping";
    if (action === "EDIT") {
      requestOptions = {
        method: "PUT",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          agnostic_industry: agnostic,
          industry_id: this.getSelectedIndustriesIds(),
          position_id: this.getSelectedPositionsId(),
          section_ids: this.getSelectedQualificationsIds(),
          deleted_industry_ids: this.getDeletedIndustries(),
          deleted_position_ids: this.getDeletedPositions(),
          deleted_section_ids: this.getDeletedQualifications(),
        }),
      };
    } else {
      requestOptions = {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          agnostic_industry: agnostic,
          industry_id: this.getSelectedIndustriesIds(),
          position_id: this.getSelectedPositionsId()[0],
          section_ids: this.getSelectedQualificationsIds(),
        }),
      };
    }

    fetch(process.env.REACT_APP_EDITOR_API_URL + url, requestOptions).then(
      (response) => {
        if (response.ok) {
          this.componentWillMount();
          return response.json();
        } else {
          alert(response.message);
          console.log(response);
        }
      }
    );
    this.hideModal();
  };

  agnosticDisabledCheck = () => {
    let disabled = null;
    for (let positon of this.state.currentSelectedPositions || []) {
      if (positon.agnosticQualification) {
        disabled = true;
        break;
      }
    }
    return disabled;
  };

  agnosticValue = () => {
    let value = null;
    for (let positon of this.state.currentSelectedPositions || []) {
      if (positon.agnosticQualification) {
        value = true;
        break;
      } else {
        value = false;
      }
    }
    return value;
  };

  getPostionsDropdown = () => {
    let {action, agnostic, agnosticPositions, unAgnosticPositions, positions} = this.state;
    if(action === "EDIT"){
      if(agnostic){
        return agnosticPositions;
      }else{
        return unAgnosticPositions;
      }
    }
    return positions;
  }

  renderAddModal() {
    let {
      showAddModel,
      currentSelectedPositions,
      currentSelectedIndustries,
      currentSelectedQualifications,
      agnostic,
      action,
    } = this.state;
    return (
      <>
        <Modal
          show={showAddModel}
          onHide={this.hideModal}
        >
          <Modal.Header closeButton>
            <Modal.Title>Add Mapping</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <form onSubmit={this.handleAddOrUpdateQualificationsMapping}>
              {/* <FormGroup>
                <label htmlFor="position_id">Position Id</label>
                <input
                  type="number"
                  className="form-control"
                  id="position_id"
                  disabled
                  value={positionId || ""}
                />
              </FormGroup> */}
              <FormGroup>
                <label htmlFor="position">Positions</label>
                <Select
                  id="position"
                  isMulti={action === "EDIT"}
                  value={currentSelectedPositions}
                  options={this.getPostionsDropdown()}
                  getOptionValue={(option) => option.id}
                  getOptionLabel={(option) => option.description}
                  onChange={(e) => {
                    if (action !== "EDIT") {
                      this.updateValue("currentSelectedPositions", [e]);
                      this.updateValue(
                        "agnostic",
                        e.agnostic_qualification
                      );
                    } else {
                      this.updateValue("currentSelectedPositions", e);
                      let agnostic = false;
                      for (let postion of e) {
                        if (postion.agnostic_qualification) {
                          agnostic = true;
                        }
                      }
                      this.updateValue("agnostic", agnostic);
                    }
                  }}
                />
              </FormGroup>

              <FormGroup>
                <label htmlFor="agnostic_ps">
                  Agnostic Qualifications
                </label>
                <input
                  id="agnostic_ps"
                  name="agnostic_ps"
                  className="form-check-input ml-5"
                  type="checkbox"
                  checked={agnostic}
                  disabled={this.agnosticDisabledCheck()}
                  onClick={() => {
                    if(!agnostic){
                      this.updateValue("currentSelectedIndustries", []);
                    }
                    this.updateValue("agnostic", !agnostic);
                  }}
                />
              </FormGroup>

              <FormGroup>
                <label htmlFor="position">Industries</label>
                <Select
                  id="position"
                  isMulti={true}
                  isDisabled={agnostic}
                  value={currentSelectedIndustries}
                  options={this.state.industries}
                  getOptionValue={(option) => option.id}
                  getOptionLabel={(option) => option.name}
                  onChange={(e) => {
                    this.updateValue("currentSelectedIndustries", e);
                  }}
                />
              </FormGroup>

              <FormGroup>
                <label htmlFor="position">Qualifications</label>
                <Select
                  id="p-s"
                  isMulti={true}
                  value={currentSelectedQualifications}
                  options={this.state.qualifications}
                  getOptionValue={(option) => option.id}
                  getOptionLabel={(option) => option.name}
                  onChange={(e) => {
                    this.updateValue("currentSelectedQualifications", e);
                  }}
                />
              </FormGroup>

              <FormGroup>
                <button className="form-control btn btn-primary" type="submit">
                  Submit
                </button>
              </FormGroup>
            </form>
          </Modal.Body>
        </Modal>
      </>
    );
  }
  actionFormatter = (cell, row) => {
    return (
      <Col col="6" sm="2" md="2" xl className="mb-3 mb-xl-0">
        <Button
          renderas="button"
          block
          outline
          color="primary"
          onClick={() => this.handleEditActions(row)}
        >
          Edit
        </Button>
      </Col>
    );
  };

  handleEditActions = (row) => {
    this.setState((prevState) => ({
      selectedRow: row,
      showAddModel: true,
      mappingId: row.id,
      currentSelectedPositions: this.getSelectedPositions(row.positionIds),
      currentSelectedIndustries: this.getSelectedIndustries(row.industryIds),
      currentSelectedQualifications: this.getSelectedQualifications(
        row.qualificationIds
      ),
      agnostic: row.agnosticQualification,
      action: "EDIT",
    }));
  };

  handleAdd = () => {
    this.setState((prevState) => ({
      showAddModel: true,
      currentSelectedPositions: [],
      currentSelectedIndustries: [],
      currentSelectedQualifications: [],
      agnostic: false,
      action: "Add",
    }));
  };

  getSelectedPositions = (positionIds) => {
    let positions = [];
    for (let position of this.state.positions) {
      if (positionIds.indexOf(position.id) !== -1) {
        positions.push(position);
      }
    }
    return positions;
  };

  getSelectedPositionsId = () => {
    let positionIds = [];
    for (let position of this.state.currentSelectedPositions) {
      positionIds.push(position.id);
    }
    return positionIds;
  };

  getSelectedPositionsNames = (row) => {
    let positionNames = [];
    for (let position of this.state.positions) {
      if (row.positionIds.indexOf(position.id) !== -1) {
        positionNames.push(position.description);
      }
    }
    return positionNames;
  };

  getSelectedIndustries = (industryIds) => {
    let industries = [];
    for (let industry of this.state.industries) {
      if (industryIds.indexOf(industry.id) !== -1) {
        industries.push(industry);
      }
    }
    return industries;
  };

  getSelectedIndustriesIds = () => {
    let industryIds = [];
    for (let industry of this.state.currentSelectedIndustries) {
      industryIds.push(industry.id);
    }
    return industryIds;
  };

  getSelectedIndustriesNames = (row) => {
    let industryNames = [];    
    for (let industry of this.state.industries) {
      if (row.industryIds.indexOf(industry.id) !== -1) {
        industryNames.push(industry.name);
      }
    }
    return industryNames;
  };

  getSelectedQualifications = (qualificationIds) => {
    let qualifications = [];
    for (let qualification of this.state.qualifications) {
      if (qualificationIds.indexOf(qualification.id) !== -1) {
        qualifications.push(qualification);
      }
    }
    return qualifications;
  };

  getSelectedQualificationsIds = () => {
    let qualificationIds = [];
    for (let qualification of this.state.currentSelectedQualifications) {
      qualificationIds.push(qualification.id);
    }
    return qualificationIds;
  };

  getSelectedQualificationsNames = (row) => {
    let qualificationNames = [];
    for (let qualification of this.state.qualifications) {
      if (row.qualificationIds.indexOf(qualification.id) !== -1) {
        qualificationNames.push(qualification.name);
      }
    }
    return qualificationNames;
  };
  getDeletedIndustries = () => {
    let selectedIndustries = this.getSelectedIndustriesIds();
    let oldIndustries = this.state.selectedRow.industryIds;
    let deletedIndustries = [];
    for (let industry of oldIndustries) {
      if (selectedIndustries.indexOf(industry) === -1) {
        deletedIndustries.push(industry);
      }
    }
    return deletedIndustries;
  };

  getDeletedPositions = () => {
    let selectedPositions = this.getSelectedPositionsId();
    let oldPositions = this.state.selectedRow.positionIds;
    let deletedPositions = [];
    for (let position of oldPositions) {
      if (selectedPositions.indexOf(position) === -1) {
        deletedPositions.push(position);
      }
    }
    return deletedPositions;
  };

  getDeletedQualifications = () => {
    let selectedQualifications = this.getSelectedQualificationsIds();
    let oldQualifications = this.state.selectedRow.qualificationIds;
    let deletedQualifications = [];
    for (let qualification of oldQualifications) {
      if (selectedQualifications.indexOf(qualification) === -1) {
        deletedQualifications.push(qualification);
      }
    }
    return deletedQualifications;
  };
  render() {
    const paginationSize = 10;
    let { qualificationsMapping } = this.state;
    let pagination = (pagination = paginationFactory());
    const { SearchBar } = Search;
    let colStyle = {
      verticalAlign: "middle",
    };

    const columns = [
      {
        id: 2,
        dataField: "positionNames",
        text: "Positions",
        style: colStyle,
        sort: true,
        //formatter: dateFormatter,
        headerStyle: {
          width: "25%",
        },
      },
      {
        id: 0,
        dataField: "agnosticQualification",
        text: "Agnostic Qualifications",
        headerStyle: {
          width: "25%",
        },
      },
      {
        id: 1,
        dataField: "industryNames",
        text: "Industries",
        style: colStyle,
        sort: true,
        //formatter: dateFormatter,
        headerStyle: {
          width: "25%",
        },
      },
      
      {
        id: 3,
        dataField: "qualificationNames",
        text: "Qualifications",
        style: colStyle,
        sort: true,
        //formatter: dateFormatter,
        headerStyle: {
          width: "25%",
        },
      },
      {
        text: "Actions",
        align: "center",
        headerStyle: {
          width: "25%",
        },
        formatter: (cell, row, index) => this.actionFormatter(cell, row),
      },
    ];

    this.loadTable = () => {
      if (this.state.loaded) {
        return (
          <ToolkitProvider
            keyField="id"
            data={qualificationsMapping}
            columns={columns}
            style={{ overflowX: "scroll" }}
            search
          >
            {(props) => (
              <div>
                <Row className="align-items-center">
                  <Col xs="12" lg="9">
                    <h2>Qualifications Mapping</h2>
                  </Col>
                  <Col xs="12" lg="3">
                    <SearchBar {...props.searchProps} />
                  </Col>
                  <Col xs="1">
                    <Button
                      renderas="button"
                      block
                      outline
                      color="primary"
                      className="action-btn float-right "
                      onClick={this.handleAdd}
                    >
                      Add
                    </Button>
                  </Col>
                </Row>
                <hr />
                <BootstrapTable {...props.baseProps} pagination={pagination} />
              </div>
            )}
          </ToolkitProvider>
        );
      }
    };

    return (
      <div className="animated fadeIn">
        <Card>
          <CardBody>
            <div className="d-none d-md-block">
              <Row className="align-items-center">
                <Col col="12" xl className="mb-3 mb-xl-0">
                  {this.loadTable()}
                </Col>
              </Row>
            </div>
            <div className="d-block d-md-none">{this.loadTable()}</div>
          </CardBody>
        </Card>
        {this.renderAddModal()}
      </div>
    );
  }
}

export default QualificationsMapping;
