import React, { Component } from "react";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import "react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit.min.css";
import "react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css";
import BootstrapTable from "react-bootstrap-table-next";
import { Button, Card, CardBody, Col, Row, FormGroup } from "reactstrap";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory from "react-bootstrap-table2-paginator";
import Modal from "react-bootstrap/Modal";
import Select from "react-select";

class PositionsMapping extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      positionsMapping: [],
      loaded: false,
      showAddModel: false,
      positions: [],
      functions: [],
    };
  }

  componentWillMount() {
    Promise.all([
      fetch(process.env.REACT_APP_EDITOR_API_URL + "/ontology/positions"),
      fetch(process.env.REACT_APP_EDITOR_API_URL + "/functions"),
    ]).then(async (res) => {
      let positions = await res[0].json();
      this.setState({
        positions: positions,
        functions: await res[1].json(),
      });
      fetch(
        process.env.REACT_APP_EDITOR_API_URL + "/ontology/positions-categories"
      )
        .then((response) => {
          if (response.ok) {
            return response.json();
          } else {
            console.log(response);
          }
        })
        .then((json) => {
          for (let record of json) {
            record.positionNames = this.getSelectedPositionsNames(record).join(
              ", "
            );
          }
          this.setState({
            positionsMapping: json,
            loaded: true,
          });
        });
    });
  }
  hideModal = () => {
    this.setState({
      showAddModel: false,
    });
  };

  updateValue = (key, value) => {
    this.setState({
      [key]: value,
    });
  };
  handleAddUpdatePostionsMapping = (event) => {
    event.preventDefault();
    let { currentSelectedFunction } = this.state;
    let requestOptions,
      url = "/ontology/positions-categories";

    requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        function_id: currentSelectedFunction.id,
        position_ids: this.getSelectedPositionsId(),
        deleted_position_ids: this.getDeletedPositions(),
      }),
    };

    fetch(process.env.REACT_APP_EDITOR_API_URL + url, requestOptions).then(
      (response) => {
        if (response.ok) {
          this.componentWillMount();
          return response.json();
        } else {
          alert(response.message);
          console.log(response);
        }
      }
    );
    this.hideModal();
  };

  renderAddModal() {
    let {
      showAddModel,
      currentSelectedPositions,
      currentSelectedFunction,
    } = this.state;
    return (
      <>
        <Modal show={showAddModel} onHide={this.hideModal}>
          <Modal.Header closeButton>
            <Modal.Title>Add Mapping</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <form onSubmit={this.handleAddUpdatePostionsMapping}>
              <FormGroup>
                <label htmlFor="position">Function</label>
                <Select
                  id="position"
                  isMulti={false}
                  isDisabled={true}
                  value={currentSelectedFunction}
                  options={this.state.positions}
                  getOptionValue={(option) => option.id}
                  getOptionLabel={(option) => option.name}
                />
              </FormGroup>
              <FormGroup>
                <label htmlFor="position">Positions</label>
                <Select
                  id="position"
                  isMulti={true}
                  value={currentSelectedPositions}
                  options={this.state.positions}
                  getOptionValue={(option) => option.id}
                  getOptionLabel={(option) => option.description}
                  onChange={(e) => {
                    this.updateValue("currentSelectedPositions", e);
                  }}
                />
              </FormGroup>

              <FormGroup>
                <button className="form-control btn btn-primary" type="submit">
                  Submit
                </button>
              </FormGroup>
            </form>
          </Modal.Body>
        </Modal>
      </>
    );
  }
  actionFormatter = (cell, row) => {
    return (
      <Col col="6" sm="2" md="2" xl className="mb-3 mb-xl-0">
        <Button
          renderas="button"
          block
          outline
          color="primary"
          onClick={() => this.handleEditActions(row)}
        >
          Edit
        </Button>
      </Col>
    );
  };

  handleEditActions = (row) => {
    this.setState((prevState) => ({
      selectedRow: row,
      showAddModel: true,
      currentSelectedPositions: row.position,
      currentSelectedFunction: {
        id: row.id,
        name: row.description,
      },
    }));
  };

  getSelectedPositionsId = () => {
    let positionIds = [];
    for (let position of this.state.currentSelectedPositions) {
      positionIds.push(position.id);
    }
    return positionIds;
  };

  getSelectedPositionsNames = (row) => {
    let positionNames = [];
    for (let position of row.position) {
      positionNames.push(position.description);
    }
    return positionNames;
  };

  getDeletedPositions = () => {
    let selectedPositions = this.getSelectedPositionsId();
    let oldPositions = this.state.selectedRow.position;
    let deletedPositions = [];
    for (let position of oldPositions) {
      if (selectedPositions.indexOf(position.id) === -1) {
        deletedPositions.push(position.id);
      }
    }
    return deletedPositions;
  };

  render() {
    const paginationSize = 10;
    let { positionsMapping } = this.state;
    let pagination = (pagination = paginationFactory());
    const { SearchBar } = Search;
    let colStyle = {
      verticalAlign: "middle",
    };

    const columns = [
      {
        id: 0,
        dataField: "description",
        text: "Category",
        headerStyle: {
          width: "25%",
        },
      },
      {
        id: 2,
        dataField: "positionNames",
        text: "Positions",
        style: colStyle,
        sort: true,
        //formatter: dateFormatter,
        headerStyle: {
          width: "25%",
        },
      },
      {
        text: "Actions",
        align: "center",
        headerStyle: {
          width: "25%",
        },
        formatter: (cell, row, index) => this.actionFormatter(cell, row),
      },
    ];

    this.loadTable = () => {
      if (this.state.loaded) {
        return (
          <ToolkitProvider
            keyField="id"
            data={positionsMapping}
            columns={columns}
            style={{ overflowX: "scroll" }}
            search
          >
            {(props) => (
              <div>
                <Row className="align-items-center">
                  <Col xs="12" lg="9">
                    <h2>Postions Categories Mapping</h2>
                  </Col>
                  <Col xs="12" lg="3">
                    <SearchBar {...props.searchProps} />
                  </Col>
                </Row>
                <hr />
                <BootstrapTable {...props.baseProps} pagination={pagination} />
              </div>
            )}
          </ToolkitProvider>
        );
      }
    };

    return (
      <div className="animated fadeIn">
        <Card>
          <CardBody>
            <div className="d-none d-md-block">
              <Row className="align-items-center">
                <Col col="12" xl className="mb-3 mb-xl-0">
                  {this.loadTable()}
                </Col>
              </Row>
            </div>
            <div className="d-block d-md-none">{this.loadTable()}</div>
          </CardBody>
        </Card>
        {this.renderAddModal()}
      </div>
    );
  }
}

export default PositionsMapping;
