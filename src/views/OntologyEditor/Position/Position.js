import React, { Component } from "react";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import "react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit.min.css";
import "react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css";
import BootstrapTable from "react-bootstrap-table-next";
import { Button, Card, CardBody, Col, Row, FormGroup } from "reactstrap";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory from "react-bootstrap-table2-paginator";
import Modal from "react-bootstrap/Modal";
import { Link } from "react-router-dom";
import StandardSelect from "../../../form_components/StandardSelect/StandardSelect";
import agnosticOptions from "../../../dropdowns/agnosticOptions";

class PositionView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      dropdownOpen: false,
      radioSelected: 2,
      position: [],
      loaded: false,
      showAddPositionModel: false,
      positionName: "",
      positionId: "",
      agnosticPandS: false,
      agnosticQualification: false,
      agnosticSkill: false,
      action: "",
    };
  }

  componentWillMount() {
    fetch(process.env.REACT_APP_EDITOR_API_URL + "/ontology/positions")
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          console.log(response);
        }
      })
      .then((json) =>
        this.setState({
          position: json,
          loaded: true,
        })
      );
  }

  hideModal = () => {
    this.setState({
      showAddPositionModel : false
    })
  }

  updateValue = (key,value) => {
    this.setState({
      [key] : value
    })
  }
  handleAddOrUpdatePosition = (event) => {
    event.preventDefault();
    let { positionName, positionId, agnosticPandS, action, agnosticQualification, agnosticSkill} = this.state;
    let requestOptions, url = "/ontology/positions";
    if(action === "EDIT"){
      requestOptions = { method: 'PUT', headers: { 'Content-Type': 'application/json' }, body: JSON.stringify({
        name : positionName
      }) };  
      url += `/${positionId}`
    }else{
      requestOptions = { method: 'POST', headers: { 'Content-Type': 'application/json' }, body: JSON.stringify({
        name : positionName,
        agnostic_products_and_services : agnosticPandS,
        agnostic_qualification : agnosticQualification,
        agnostic_skill : agnosticSkill
      }) };  
    }
    

    fetch(process.env.REACT_APP_EDITOR_API_URL + url, requestOptions)
      .then(response => {
        if (response.ok) {
          this.componentWillMount();
          return response.json();
        } else {
          alert(response.message);
          console.log(response);
        }
      });
    this.hideModal();
  }

  renderAddPositionModal() {
    let { showAddPositionModel, positionName, positionId, agnosticPandS, action, agnosticQualification, agnosticSkill} = this.state;
    return (
      <>
        <Modal show={showAddPositionModel} onHide={this.hideModal}>
          <Modal.Header closeButton >
            <Modal.Title>Add Position</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <form onSubmit={this.handleAddOrUpdatePosition}>
              {/* <FormGroup>
                <label htmlFor="position_id">Position Id</label>
                <input
                  type="number"
                  className="form-control"
                  id="position_id"
                  disabled
                  value={positionId || ""}
                />
              </FormGroup> */}
              <FormGroup>
                <label htmlFor="add_position_name">Position Name</label>
                <input
                  type="text"
                  className="form-control"
                  id="add_position_name"
                  value={positionName}
                  onChange={(e) => {this.updateValue("positionName",e.target.value)}}
                />
              </FormGroup>

              <FormGroup>
              
                <label htmlFor="agnostic_ps">
                  Agnostic Products And Service
                </label>
                <StandardSelect
                  className="mb-3"                  
                  placeholder="Select Agnostic Products And Service"
                  options={agnosticOptions}
                  value={agnosticPandS}
                  disabled={action === "EDIT" ? true : false}
                  onChange={(v) => {this.updateValue("agnosticPandS",v)}}
                />                
                
              </FormGroup>
              <FormGroup>
                <label htmlFor="add_position_name">
                  Agnostic Qualification
                </label>
                <StandardSelect
                  className="mb-3"                  
                  placeholder="Select Agnostic Qualification"
                  options={agnosticOptions}
                  value={agnosticQualification}
                  disabled={action === "EDIT" ? true : false}
                  onChange={(v) => {this.updateValue("agnosticQualification",v)}}
                />                                
              </FormGroup>
              <FormGroup>
                <label htmlFor="add_position_name">Agnostic Skill</label>
                <StandardSelect
                  className="mb-3"                  
                  placeholder="Select Agnostic Skill"
                  options={agnosticOptions}
                  value={agnosticSkill}
                  disabled={action === "EDIT" ? true : false}
                  onChange={(v) => {this.updateValue("agnosticSkill",v)}}
                />                                                
              </FormGroup>
              <FormGroup>
                <button className="form-control btn btn-primary" type="submit">
                  Submit
                </button>
              </FormGroup>
            </form>
          </Modal.Body>
        </Modal>
      </>
    );
  }

  handleEditActions = (row) => {    
    this.setState((prevState) => ({
      showAddPositionModel: true,
      positionName: row.description,
      positionId: row.id,
      agnosticPandS: row.agnostic_products_and_services,
      agnosticQualification: row.agnostic_qualification,
      agnosticSkill: row.agnostic_skill,
      action: "EDIT",
    }));
  };

  handleAdd = () => {    
    this.setState((prevState) => ({
      showAddPositionModel: true,
      positionName: "",
      positionId: "",
      agnosticPandS: null,
      agnosticQualification: null,
      agnosticSkill: null,
      action: "Add",
    }));
  };

  actionFormatter = (cell, row) => {
    return (
      <Col col="6" sm="2" md="2" xl className="mb-3 mb-xl-0">
        <Button
          renderas="button"
          block
          outline
          color="primary"
          onClick={() => this.handleEditActions(row)}
        >
          Edit
        </Button>
      </Col>
    );
  };

  render() {
    const paginationSize = 10;
    let { position } = this.state;
    let pagination = (pagination = paginationFactory());
    const { SearchBar } = Search;
    let colStyle = {
      verticalAlign: "middle",
    };

    const columns = [
      {
        id: 0,
        dataField: "id",
        text: "Position Id",
        headerStyle: {
          width: "10%",
        },
      },
      {
        id: 1,
        dataField: "description",
        text: "Position Name",
        style: colStyle,
        sort: true,
        //formatter: dateFormatter,
        headerStyle: {
          width: "25%",
        },
      },
      {
        id: 2,
        dataField: "agnostic_products_and_services",
        text: "Agnostic Products And Service",
        style: colStyle,
        sort: true,
        headerStyle: {
          width: "25%",
        },
      },
      {
        id: 3,
        dataField: "agnostic_qualification",
        text: "Agnostic Qualification",
        style: colStyle,
        sort: true,
        headerStyle: {
          width: "15%",
        },
      },
      {
        id: 4,
        dataField: "agnostic_skill",
        text: "Agnostic Qualification",
        style: colStyle,
        sort: true,
        headerStyle: {
          width: "25%",
        },
      },
      {
        text: "Actions",
        align: "center",
        headerStyle: {
          width: "25%",
        },
        formatter: (cell, row, index) => this.actionFormatter(cell, row),
      },
    ];

    this.loadTable = () => {
      if (this.state.loaded) {
        return (
          <ToolkitProvider
            keyField="id"
            data={position}
            columns={columns}
            style={{ overflowX: "scroll" }}
            search
          >
            {(props) => (
              <div>
                <Row className="align-items-center">
                  <Col xs="12" lg="9">
                    <h2>Position</h2>
                  </Col>
                  <Col xs="12" lg="3">
                    <SearchBar {...props.searchProps} />
                  </Col>
                  <Col xs="1">
                    
                      <Button
                        renderas="button"
                        block
                        outline
                        color="primary"
                        className="action-btn float-right "
                        onClick={this.handleAdd}
                      >
                        Add
                      </Button>                    
                  </Col>
                </Row>
                <hr />
                <BootstrapTable {...props.baseProps} pagination={pagination} />
              </div>
            )}
          </ToolkitProvider>
        );
      }
    };

    return (
      <div className="animated fadeIn">
        <Card>
          <CardBody>
            <div className="d-none d-md-block">
              <Row className="align-items-center">
                <Col col="12" xl className="mb-3 mb-xl-0">
                  {this.loadTable()}
                </Col>
              </Row>
            </div>
            <div className="d-block d-md-none">{this.loadTable()}</div>
          </CardBody>
        </Card>
        {this.renderAddPositionModal()}
      </div>
    );
  }
}

export default PositionView;
