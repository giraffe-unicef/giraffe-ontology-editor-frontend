export default [{
    value : null,
    label : "Undecided"
},{
    value : true,
    label : "Agnostic"
},{
    value : false,
    label : "Not Agnostic"
}]