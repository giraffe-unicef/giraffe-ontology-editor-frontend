import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import {  
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Nav,  
  UncontrolledDropdown,
} from "reactstrap";
import PropTypes from "prop-types";

import { AppNavbarBrand } from "@coreui/react";
import logo from "../../assets/img/brand/logo.svg";
import ico from "../../assets/img/brand/ico.svg";
import NavItem from "reactstrap/lib/NavItem";


const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component {
  render() {            
      let nav = (
        <Nav className="d-md-down" navbar>
          <NavItem className="px-3">
            <Link to="/ontology/position" className="nav-link">
              Position
            </Link>
          </NavItem>
          <NavItem className="px-3">
            <Link to="/ontology/product-service" className="nav-link">
            Products And Services
            </Link>
          </NavItem>
          <NavItem className="px-3">
            <Link to="/ontology/qualification" className="nav-link">
            Qualifications
            </Link>
          </NavItem>
          <NavItem className="px-3">
            <Link to="/ontology/skills" className="nav-link">
            Skills
            </Link>
          </NavItem>
          <NavItem className="px-3">
            <Link to="/ontology/products-services-mapping" className="nav-link">
            Products and Services Mapping
            </Link>
          </NavItem>
          <NavItem className="px-3">
            <Link to="/ontology/qualifications-mapping" className="nav-link">
            Qualifications Mapping
            </Link>
          </NavItem>
          <NavItem className="px-3">
            <Link to="/ontology/skills-mapping" className="nav-link">
            Skills Mapping
            </Link>
          </NavItem>
          <NavItem className="px-3">
            <Link to="/ontology/positions-mapping" className="nav-link">
            Positions Mapping
            </Link>
          </NavItem>          
        </Nav>
      );
    
    return (
      <React.Fragment>
        {/*<AppSidebarToggler className="d-lg-none" display="md" mobile />*/}
        <div className="d-md-down-none">
          <AppNavbarBrand
            full={{ src: logo, width: 300, height: 45, alt: "Giraffe logo" }}
            minimized={{ src: ico, width: 30, height: 30, alt: "Giraffe Logo" }}
          />
        </div>

        {nav}
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default withRouter(DefaultHeader);
